import Sequelize from 'sequelize';

import Usuario from '../app/models/Usuario';
import databaseConfig from '../config/database';
import Cliente from '../app/models/cliente/Cliente';
import ClienteComplementar from '../app/models/cliente/ClienteComplementar';
import ClienteDefaults from '../app/models/cliente/ClienteDefaults';
import CondicaoPagamento from '../app/models/cliente/CondicaoPagamento';
import Movimento from '../app/models/movimento/Movimento';
import MovimentoItem from '../app/models/movimento/MovimentoItem';
import Romaneio from '../app/models/movimento/Romaneio/Romaneio';
import Financeiro from '../app/models/financeiro/Financeiro';
import AutoIncremento from '../app/models/parametros/AutoIncremento';
import Produto from '../app/models/produto/Produto';
import ProdutoDef from '../app/models/produto/ProdutoDef';

import TabelaPreco from '../app/models/produto/TabelaPreco/TabelaPreco';
import Rota from '../app/models/rota/Rota';

const models = [
  Usuario,
  Cliente,
  ClienteDefaults,
  ClienteComplementar,
  CondicaoPagamento,
  Movimento,
  Romaneio,
  MovimentoItem,
  Financeiro,
  AutoIncremento,
  Produto,
  ProdutoDef,
  TabelaPreco,
  Rota,
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}
export default new Database();
