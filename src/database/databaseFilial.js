import Sequelize from 'sequelize';

import databaseFilial from '../config/databaseFilial';

import Cliente from '../app/models/filial/cliente/Cliente';

import ClienteComplementar from '../app/models/filial/cliente/ClienteComplementar';
import ClienteDefaults from '../app/models/filial/cliente/ClienteDefaults';
import CondicaoPagamento from '../app/models/filial/cliente/CondicaoPagamento';
import MovimentoFilial from '../app/models/filial/movimento/Movimento';
import MovimentoItem from '../app/models/filial/movimento/MovimentoItem';

import FechamentoNota from '../app/models/filial/movimento/entrada/FechamentoNota';
import FechamentoItem from '../app/models/filial/movimento/entrada/FechamentoItem';

import Romaneio from '../app/models/filial/movimento/Romaneio/Romaneio';
import Financeiro from '../app/models/filial/financeiro/Financeiro';
import AutoIncremento from '../app/models/filial/parametros/AutoIncremento';

import Produto from '../app/models/filial/produto/Produto';
import SaldoProduto from '../app/models/filial/produto/SaldoProduto';
import ProdutoDef from '../app/models/filial/produto/ProdutoDef';
import ProdutoComposto from '../app/models/filial/produto/ProdutoComposto';
import ProdutoComplementar from '../app/models/filial/produto/ProdutoComplementar';

import Rota from '../app/models/filial/rota/Rota';

const models = [
  Cliente,
  ClienteDefaults,
  ClienteComplementar,
  CondicaoPagamento,
  MovimentoFilial,
  FechamentoNota,
  FechamentoItem,
  Romaneio,
  MovimentoItem,
  Financeiro,
  AutoIncremento,
  Produto,
  ProdutoDef,
  SaldoProduto,
  ProdutoComposto,
  ProdutoComplementar,
  Rota,
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseFilial);
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}
export default new Database();
