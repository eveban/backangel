/** Configuração para funcionar o dotenv com PM2 - mover .env para diretório */
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });
require('dotenv').config();
// import 'dotenv/config';

// console.log(process.env);
// eslint-disable-next-line import/order
import app from './app';

app.listen(3334, () => {
  console.log('🚀 Back-end started!');
});
