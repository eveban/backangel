import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class Movimento extends Model {
  static init(sequelize) {
    super.init(
      {
        coligada: {
          type: Sequelize.INTEGER,
          field: 'CODCOLIGADA',
        },
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDMOV',
        },

        filial: {
          type: Sequelize.INTEGER,
          field: 'CODFILIAL',
        },

        dataEmissao: {
          type: TYPES.DateTime,
          field: 'DATAEMISSAO',
        },

        codTipoMovimento: {
          type: Sequelize.STRING(10),
          field: 'CODTMV',
          default: '2.1.10',
        },
        localEstoque: {
          type: Sequelize.STRING(10),
          field: 'CODLOC',
        },
        numeroMovimento: {
          type: Sequelize.STRING(10),
          field: 'NUMEROMOV',
        },
        serie: {
          type: Sequelize.STRING(5),
          field: 'SERIE',
        },

        dataCriacao: {
          type: TYPES.DateTime,
          field: 'DATACRIACAO',
        },
        integraAplicacao: {
          type: Sequelize.CHAR,
          field: 'INTEGRAAPLICACAO',
          defaultValue: 'T',
        },
        tipo: {
          type: Sequelize.CHAR,
          field: 'TIPO',
          defaultValue: 'P',
        },

        status: {
          type: Sequelize.CHAR,
        },

        dataSaida: {
          type: TYPES.DateTime,
          field: 'DATASAIDA',
        },

        dataMovimento: {
          type: TYPES.DateTime,
          field: 'DATAMOVIMENTO',
        },

        codigoUsuario: {
          type: Sequelize.STRING(30),
          field: 'CODUSUARIO',
        },

        codcfo: {
          type: Sequelize.STRING(10),
          field: 'CODCFO',
        },

        codcoligadacfo: {
          type: Sequelize.INTEGER,
          field: 'CODCOLCFO',
        },
        valorBruto: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'VALORBRUTO',
        },
        valorOutros: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'VALOROUTROS',
        },
        valorLiquido: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'VALORLIQUIDO',
        },
        codMonetario: {
          type: Sequelize.STRING,
          field: 'CODMOEVALORLIQUIDO',
          defaultValue: 'R$',
        },
        codCondPagamento: {
          type: Sequelize.STRING(10),
          field: 'CODCPG',
        },

        codcoligadacfoaux: {
          type: Sequelize.INTEGER,
          field: 'CODCOLCFOAUX',
        },
        codcfoaux: {
          type: Sequelize.STRING(10),
          field: 'CODCFOAUX',
        },
        usuarioCriacao: {
          type: Sequelize.STRING(30),
          field: 'USUARIOCRIACAO',
        },

        dataEntrega: {
          type: TYPES.DateTime,
          field: 'DATAENTREGA',
        },

        prazoEntrega: {
          type: Sequelize.INTEGER,
          field: 'PRAZOENTREGA',
        },

        codRepresentante: {
          type: Sequelize.STRING(20),
          field: 'CODRPR',
        },
        romaneio: {
          type: Sequelize.STRING(10),
          field: 'CODTB5FAT',
        },
      },
      {
        tableName: 'TMOV',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Cliente, {
      foreignKey: 'codcfo',
      as: 'cliente',
    });

    // this.belongsTo(models.Romaneio, {
    //   foreignKey: 'id',
    //   as: 'romaneio',
    // });

    this.hasMany(models.MovimentoItem, {
      foreignKey: 'idmov',
      as: 'items',
      onDelete: 'CASCADE',
    });
    // MovimentoItem.belongsTo(Movimento, {
    //   foreignKey: 'idmov',
    //   as: 'item',
    // });
  }
}

export default Movimento;
