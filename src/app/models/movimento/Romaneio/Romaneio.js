import Sequelize, { Model } from 'sequelize';

class Romaneio extends Model {
  static init(sequelize) {
    super.init(
      {
        coligada: {
          type: Sequelize.INTEGER,
          field: 'CODCOLIGADA',
        },
        id: {
          type: Sequelize.STRING,
          primaryKey: true,
          field: 'CODTB5FAT',
        },
        descricao: {
          type: Sequelize.STRING(120),
          field: 'DESCRICAO',
        },
      },
      {
        tableName: 'TTB5',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}

export default Romaneio;
