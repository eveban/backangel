import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class MovimentoItem extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDMOV',
        },
        nseqitmmov: {
          type: Sequelize.INTEGER,
          field: 'NSEQITMMOV',
        },
        idprd: {
          type: Sequelize.INTEGER,
          field: 'IDPRD',
        },
        coligada: {
          type: Sequelize.INTEGER,
          field: 'CODCOLIGADA',
          primaryKey: true,
        },
        precoUnitario: {
          type: Sequelize.DECIMAL,
          field: 'PRECOUNITARIO',
        },
        valorTotalItem: {
          type: Sequelize.DECIMAL,
          field: 'VALORTOTALITEM',
        },
        codigoUnidade: {
          type: Sequelize.STRING,
          field: 'CODUND',
        },
        quantidade: {
          type: Sequelize.INTEGER,
          field: 'QUANTIDADE',
        },

        numeroSequencial: {
          type: Sequelize.INTEGER,
          field: 'NUMEROSEQUENCIAL',
        },
        codloc: {
          type: Sequelize.STRING,
          field: 'CODLOC',
        },

        filial: {
          type: Sequelize.INTEGER,
          field: 'CODFILIAL',
        },

        dataEmissao: {
          type: TYPES.DateTime,
          field: 'DATAEMISSAO',
        },
        dataEntrega: {
          type: TYPES.DateTime,
          field: 'DATAENTREGA',
        },
        quantidadeOriginal: {
          type: Sequelize.INTEGER,
          field: 'QUANTIDADEORIGINAL',
        },
        quantidadeTotal: {
          type: Sequelize.INTEGER,
          field: 'QUANTIDADETOTAL',
        },
        quantidadeReceber: {
          type: Sequelize.INTEGER,
          field: 'QUANTIDADEARECEBER',
        },
        codtb2fat: {
          type: Sequelize.STRING,
          field: 'CODTB2FAT',
        },
        codtb3fat: {
          type: Sequelize.STRING,
          field: 'CODTB3FAT',
        },
        codtb4fat: {
          type: Sequelize.STRING,
          field: 'CODTB4FAT',
        },
        campoLivre: {
          type: Sequelize.STRING,
          field: 'CAMPOLIVRE',
        },
      },
      {
        tableName: 'TITMMOV',
        freezeTableName: true,
        hasTrigger: false,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Movimento, {
      foreignKey: 'idmov',
      as: 'movimento',
    });
    this.belongsTo(models.Produto, {
      foreignKey: 'idprd',
      as: 'produto',
    });
  }
}

export default MovimentoItem;
