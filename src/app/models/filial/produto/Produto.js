import Sequelize, { Model } from 'sequelize';

class Produto extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },

        codigoprd: {
          type: Sequelize.STRING,
          field: 'CODIGOPRD',
        },
        descricao: {
          type: Sequelize.STRING,
          field: 'DESCRICAO',
        },
        fantasia: {
          type: Sequelize.STRING,
          field: 'NOMEFANTASIA',
        },
        descAuxiliar: {
          type: Sequelize.STRING,
          field: 'DESCRICAOAUX',
        },
        codigoAuxiliar: {
          type: Sequelize.STRING,
          field: 'CODIGOAUXILIAR',
        },

        pesoLiquido: {
          type: Sequelize.DECIMAL,
          field: 'PESOLIQUIDO',
        },
        pesoMedio: {
          type: Sequelize.DECIMAL,
          field: 'CAMPOLIVRE3',
        },
        pesoTotal: {
          type: Sequelize.VIRTUAL,
        },

        inativo: {
          type: Sequelize.SMALLINT,
          field: 'INATIVO',
        },
      },
      {
        tableName: 'TPRODUTO',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.ProdutoDef, {
      foreignKey: 'id',
      as: 'defaults',
    });
    this.belongsTo(models.SaldoProduto, {
      foreignKey: 'id',
      as: 'saldo',
    });
    this.belongsTo(models.ProdutoComposto, {
      foreignKey: 'id',
      as: 'composto',
    });
    this.belongsTo(models.ProdutoComplementar, {
      foreignKey: 'id',
      as: 'complementar',
    });
  }
}

export default Produto;
