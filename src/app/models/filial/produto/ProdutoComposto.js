import Sequelize, { Model } from 'sequelize';

class ProdutoComposto extends Model {
  static init(sequelize) {
    super.init(
      {
        codColigada: {
          type: Sequelize.INTEGER,
          field: 'CODCOLIGADA',
        },
        id: {
          type: Sequelize.INTEGER,
          // primaryKey: true,
          field: 'IDPRD',
        },
        idProdutoChild: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRDCOMPONENTE',
        },

        numeroSequencial: {
          type: Sequelize.INTEGER,
          field: 'NUMEROSEQUENCIAL',
        },
        quantidade: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'QUANTIDADE',
        },
        codigoUnidade: {
          type: Sequelize.STRING,
          field: 'CODUND',
        },
      },

      {
        tableName: 'TPRDCOMPOSTO',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.hasOne(models.Produto, {
      foreignKey: 'id',
      as: 'composto',
    });
  }
}

export default ProdutoComposto;
