import Sequelize, { Model } from 'sequelize';

class ProdutoComplementar extends Model {
  static init(sequelize) {
    super.init(
      {
        codColigada: {
          type: Sequelize.INTEGER,
          field: 'CODCOLIGADA',
        },
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },

        percentualA: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'PERCENTUAL_A',
        },
        percentualB: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'PERCENTUAL_B',
        },
        percentualC: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'PERCENTUAL_C',
        },
        percentualD: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'PERCENTUAL_D',
        },
        percentualE: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'PERCENTUAL_E',
        },
      },

      {
        tableName: 'TPRDCOMPL',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.hasOne(models.Produto, {
      foreignKey: 'id',
      as: 'complementar',
    });
  }
}

export default ProdutoComplementar;
