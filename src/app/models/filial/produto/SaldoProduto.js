import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class SaldoProduto extends Model {
  static init(sequelize) {
    super.init(
      {
        idProduto: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },
        codColigada: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'CODCOLIGADA',
        },
        codFilial: {
          type: Sequelize.INTEGER,
          field: 'CODFILIAL',
        },
        codLocalEstoque: {
          type: Sequelize.STRING,
          field: 'CODLOC',
        },
        custoMedio: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'CUSTOMEDIO',
        },
        custoUnitario: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'CUSTOUNITARIO',
        },
        saldoFisico02: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'SALDOFISICO2',
        },
        saldoFisico10: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'SALDOFISICO10',
        },

        dataCustoUnitario: {
          type: TYPES.DateTime,
          field: 'DATACUSTOUNITARIO',
        },
        dataCustoMedio: {
          type: TYPES.DateTime,
          field: 'DATACUSTOMEDIO',
        },
      },

      {
        tableName: 'TPRDLOC',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}

export default SaldoProduto;
