import Sequelize, { Model } from 'sequelize';

class Financeiro extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDLAN',
        },
        codcfo: {
          type: Sequelize.STRING,
          field: 'CODCFO',
        },
        valorOriginal: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'VALORORIGINAL',
        },
        status: {
          type: Sequelize.INTEGER,
          field: 'STATUSLAN',
        },
        codcoligadacfo: {
          type: Sequelize.STRING(10),
          field: 'CODCOLCFO',
        },
        pagrec: {
          type: Sequelize.INTEGER,
          field: 'PAGREC',
        },
        codrpr: {
          type: Sequelize.STRING,
          field: 'CODRPR',
        },
        vencimento: {
          type: Sequelize.DATEONLY,
          field: 'DATAVENCIMENTO',
        },
        codcxa: {
          type: Sequelize.STRING,
          field: 'CODCXA',
        },
        nfoudup: {
          type: Sequelize.INTEGER,
          field: 'NFOUDUP',
        },
      },
      { tableName: 'FLAN', freezeTableName: true, sequelize }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Cliente, {
      foreignKey: 'codcfo',
      as: 'cliente',
    });

    // this.hasMany(models.MovimentoItem, {
    //   foreignKey: 'idmov',
    //   as: 'items',
    //   onDelete: 'CASCADE',
    // });
  }
}

export default Financeiro;
