import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class FechamentoNota extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'ID',
        },
        idPrd: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },

        dataAbate: {
          type: TYPES.DateTime,
          field: 'DATAABATE',
        },
        dataFechamento: {
          type: TYPES.DateTime,
          field: 'DATAFECHAMENTO',
        },
        codigoAuxiliar: {
          type: Sequelize.STRING(10),
          field: 'CODIGOAUXILIAR',
        },

        quantidade: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'QUANTIDADE',
        },

        pesoBruto: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'PESOBRUTO',
        },

        pesoRetalho: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'PESORETALHO',
        },

        pesoLiquido: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'PESOLIQUIDO',
        },

        fatorTraseiro: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'FATORTRASEIRO',
        },
        fatorDianteiro: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'FATORDIANTEIRO',
        },
        fatorPontaAgulha: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'FATORPONTAAGULHA',
        },

        createdAt: {
          type: TYPES.DateTime,
          field: 'RECCREATEDDON',
        },

        updatedAt: {
          type: TYPES.DateTime,
          field: 'RECMODIFIEDBY',
        },
      },
      {
        tableName: 'ZTFECHAMENTONOTAS',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    //   this.belongsTo(models.Cliente, {
    //     foreignKey: 'codcfo',
    //     as: 'cliente',
    //   });

    // this.belongsTo(models.Romaneio, {
    //   foreignKey: 'id',
    //   as: 'romaneio',
    // });

    this.hasMany(models.FechamentoItem, {
      foreignKey: 'idFechamento',
      as: 'itens',
      onDelete: 'CASCADE',
    });
    // MovimentoItem.belongsTo(Movimento, {
    //   foreignKey: 'idmov',
    //   as: 'item',
    // });
  }
}

export default FechamentoNota;
