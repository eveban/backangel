import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class FechamentoItem extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'ID',
        },
        idFechamento: {
          type: Sequelize.INTEGER,
          field: 'ID_FECHAMENTO',
        },
        idPrdPai: {
          type: Sequelize.INTEGER,
          field: 'IDPRDPAI',
        },
        idPrd: {
          type: Sequelize.INTEGER,
          field: 'IDPRD',
        },

        dataAbate: {
          type: TYPES.DateTime,
          field: 'DATAABATE',
        },
        dataFechamento: {
          type: TYPES.DateTime,
          field: 'DATAFECHAMENTO',
        },

        quantidade: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'QUANTIDADE',
        },

        peso: {
          type: Sequelize.DECIMAL(18, 4),
          field: 'PESO',
        },

        createdAt: {
          type: TYPES.DateTime,
          field: 'RECCREATEDDON',
        },

        updatedAt: {
          type: TYPES.DateTime,
          field: 'RECMODIFIEDBY',
        },
      },
      {
        tableName: 'ZTFECHAMENTOITENS',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  // static associate(models) {
  //   this.belongsTo(models.Cliente, {
  //     foreignKey: 'codcfo',
  //     as: 'cliente',
  //   });

  // this.belongsTo(models.Romaneio, {
  //   foreignKey: 'id',
  //   as: 'romaneio',
  // });

  // this.hasMany(models.MovimentoItem, {
  //   foreignKey: 'idmov',
  //   as: 'items',
  //   onDelete: 'CASCADE',
  // });
  // MovimentoItem.belongsTo(Movimento, {
  //   foreignKey: 'idmov',
  //   as: 'item',
  // });
  // }
}

export default FechamentoItem;
