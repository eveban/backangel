import Sequelize, { Model } from 'sequelize';

class ClienteComplementar extends Model {
  static init(sequelize) {
    super.init(
      {
        codcfo: {
          type: Sequelize.STRING,
          primaryKey: true,
        },
        limite: {
          type: Sequelize.DECIMAL(10, 2),
          field: 'LIMITE',
        },
        motivoBloqueio: {
          type: Sequelize.STRING,
          field: 'MOTIVOBLOQUEIO',
        },
        latitude: {
          type: Sequelize.STRING(30),
          field: 'latitude',
        },
        longitude: {
          type: Sequelize.STRING(30),
          field: 'longitude',
        },
        prioridade: {
          type: Sequelize.INTEGER,
        },
        vendedor: {
          type: Sequelize.STRING,
          field: 'vendedor',
        },
      },
      {
        tableName: 'FCFOCOMPL',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}
export default ClienteComplementar;
