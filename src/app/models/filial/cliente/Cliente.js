import Sequelize, { Model } from 'sequelize';

class Cliente extends Model {
  static init(sequelize) {
    super.init(
      {
        codcfo: {
          type: Sequelize.STRING,
          primaryKey: true,
        },

        nome: {
          type: Sequelize.STRING,
          field: 'NOME',
        },
        cgccfo: {
          type: Sequelize.STRING,
          field: 'CGCCFO',
        },
        rua: {
          type: Sequelize.STRING,
          field: 'RUA',
        },
        numero: {
          type: Sequelize.STRING,
          field: 'NUMERO',
        },
        bairro: {
          type: Sequelize.STRING,
          field: 'BAIRRO',
        },
        cidade: {
          type: Sequelize.STRING,
          field: 'CIDADE',
        },
        ativo: {
          type: Sequelize.SMALLINT,
          field: 'ATIVO',
        },
        bloqueado: {
          type: Sequelize.SMALLINT,
          field: 'CFOIMOB',
        },
        valorop3: {
          type: Sequelize.SMALLINT,
          field: 'VALOROP3',
        },
        receber: {
          type: Sequelize.SMALLINT,
          field: 'PAGREC',
        },
        campoalfaop2: {
          type: Sequelize.STRING,
          field: 'CAMPOALFAOP2',
        },
        campoLivre: {
          type: Sequelize.STRING,
          field: 'CAMPOLIVRE',
        },
      },
      {
        tableName: 'FCFO',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.ClienteComplementar, {
      foreignKey: 'codcfo',
      as: 'complementar',
    });
    this.belongsTo(models.ClienteDefaults, {
      foreignKey: 'codcfo',
      as: 'defaults',
    });
    this.belongsTo(models.CondicaoPagamento, {
      foreignKey: 'codcfo',
      as: 'condicaoPagamento',
    });
    this.hasMany(models.Movimento, {
      foreignKey: 'codcfo',
      as: 'movimentos',
    });
  }
}

export default Cliente;
