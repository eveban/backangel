import Sequelize, { Model } from 'sequelize';

class ClienteDefaults extends Model {
  static init(sequelize) {
    super.init(
      {
        codcfo: {
          type: Sequelize.STRING,
          primaryKey: true,
        },
        representante: {
          type: Sequelize.STRING,
          field: 'CODRPR',
        },
        condicaoPagamento: {
          type: Sequelize.STRING,
          field: 'CODCPGVENDA',
        },
      },
      {
        tableName: 'FCFODEF',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}

export default ClienteDefaults;
