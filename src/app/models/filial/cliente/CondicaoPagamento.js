import Sequelize, { Model } from 'sequelize';

class CondicaoPagamento extends Model {
  static init(sequelize) {
    super.init(
      {
        codcfo: {
          type: Sequelize.STRING,
          primaryKey: true,
        },

        condicaoPagamento: {
          type: Sequelize.STRING,
          field: 'CODCPGVENDA',
        },
      },
      {
        tableName: 'TCPGFCFO',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}
export default CondicaoPagamento;
