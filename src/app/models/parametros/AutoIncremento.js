import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class AutoIncremento extends Model {
  static init(sequelize) {
    super.init(
      {
        codAutoInc: {
          type: Sequelize.INTEGER,
          field: 'CODAUTOINC',
          primaryKey: true,
        },
        codSistema: {
          type: TYPES.Char,
          field: 'CODSISTEMA',
        },
        valAutoInc: {
          type: TYPES.Int,
          field: 'VALAUTOINC',
        },
        codColigada: {
          type: TYPES.Int,
          field: 'CODCOLIGADA',
        },
      },
      {
        sequelize,
        tableName: 'GAUTOINC',
        freezeTableName: true,
      }
    );
    return this;
  }
}

export default AutoIncremento;
