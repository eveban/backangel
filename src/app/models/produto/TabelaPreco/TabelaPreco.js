import Sequelize, { Model } from 'sequelize';
import { TYPES } from 'tedious';

class TabelaPreco extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },

        idtabpreco: {
          type: Sequelize.INTEGER,
        },

        preco: {
          type: Sequelize.DECIMAL,
          field: 'PRECO',
        },
        dataVigencia: {
          type: TYPES.DateTime,
          field: 'DATAVIGENCIAINI',
        },
      },
      {
        tableName: 'TTABPRECOPRD',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}

export default TabelaPreco;
