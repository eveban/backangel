import Sequelize, { Model } from 'sequelize';

class ProdutoDef extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },

        preco1: {
          type: Sequelize.DECIMAL,
          field: 'PRECO1',
        },
        unidadeControle: {
          type: Sequelize.STRING,
          field: 'CODUNDCONTROLE',
        },
        codtb2fat: {
          type: Sequelize.STRING,
          field: 'CODTB2FAT',
        },
        codtb3fat: {
          type: Sequelize.STRING,
          field: 'CODTB3FAT',
        },
        codtb4fat: {
          type: Sequelize.STRING,
          field: 'CODTB4FAT',
        },
        codigoColigada: {
          type: Sequelize.STRING,
          field: 'CODCOLIGADA',
        },
      },
      {
        tableName: 'TPRODUTODEF',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }
}

export default ProdutoDef;
