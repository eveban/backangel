import Sequelize, { Model } from 'sequelize';

class Produto extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          field: 'IDPRD',
        },

        codigoprd: {
          type: Sequelize.STRING,
          field: 'CODIGOPRD',
        },
        descricao: {
          type: Sequelize.STRING,
          field: 'DESCRICAO',
        },
        fantasia: {
          type: Sequelize.STRING,
          field: 'NOMEFANTASIA',
        },
        descAuxiliar: {
          type: Sequelize.STRING,
          field: 'DESCRICAOAUX',
        },
        codigoAuxiliar: {
          type: Sequelize.STRING,
          field: 'CODIGOAUXILIAR',
        },

        pesoLiquido: {
          type: Sequelize.DECIMAL,
          field: 'PESOLIQUIDO',
        },
        inativo: {
          type: Sequelize.SMALLINT,
          field: 'INATIVO',
        },
      },
      {
        tableName: 'TPRODUTO',
        freezeTableName: true,
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.ProdutoDef, {
      foreignKey: 'id',
      as: 'defaults',
    });
    this.belongsTo(models.TabelaPreco, {
      foreignKey: 'id',
      as: 'items',
    });
  }
}

export default Produto;
