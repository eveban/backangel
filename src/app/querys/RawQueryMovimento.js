export const findMovimentosToDataUsuario = (dataEmissao, usuario) => {
  const query = `
    SELECT
        T.DATAEMISSAO as dataEmissao,
        T.NUMEROMOV as id,
        T.CODCFO as codcfo,
        C.NOMEFANTASIA as fantasia,
        P.CODIGOAUXILIAR as codigoAuxiliar,
        P.DESCRICAO as produto,
        I.QUANTIDADE as quantidade,
        I.CODUND as unidade,
        I.PRECOUNITARIO as precoUnitario
    FROM
        TMOV T (NOLOCK)
        INNER JOIN FCFO C (NOLOCK) ON T.CODCFO = C.CODCFO
        INNER JOIN TITMMOV I (NOLOCK) ON I.IDMOV = T.IDMOV
        INNER JOIN TPRODUTO P (NOLOCK) ON P.IDPRD = I.IDPRD
    WHERE
        T.DATAEMISSAO = '${dataEmissao}'
        AND T.USUARIOCRIACAO = '${usuario}'
        AND T.CODTMV = '2.1.10'
        AND T.STATUS != 'C'
   `;

  return query;
};
