import * as Yup from 'yup';
import { Op } from 'sequelize';

import { subDays, format } from 'date-fns';
import moment from 'moment';

import fs from 'fs';
import path from 'path';
import puppeteer from 'puppeteer';
import handlebars from 'handlebars';

import MovimentoFilial from '../models/filial/movimento/Movimento';
import MovimentoItemFilial from '../models/filial/movimento/MovimentoItem';
import ClienteFilial from '../models/filial/cliente/Cliente';

import Produto from '../models/filial/produto/Produto';
import Movimento from '../models/movimento/Movimento';
import MovimentoItem from '../models/movimento/MovimentoItem';

import Financeiro from '../models/financeiro/Financeiro';
import FinanceiroFilial from '../models/filial/financeiro/Financeiro';

import Cliente from '../models/cliente/Cliente';
import ClienteComplementar from '../models/cliente/ClienteComplementar';
import ClienteDefaults from '../models/cliente/ClienteDefaults';
import CondicaoPagamento from '../models/cliente/CondicaoPagamento';
import AutoIncremento from '../models/parametros/AutoIncremento';

import { findMovimentosToDataUsuario } from '../querys/RawQueryMovimento';

class MovimentoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      codcfo: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }
    const {
      id,
      numeroMov,
      condicaoPagamento,
      representante,
      usuario,
      romaneio,
      ...data
    } = req.body;

    const info = await Movimento.create({
      coligada: 1,
      id,
      numeroMovimento: numeroMov,
      filial: 1,
      romaneio,
      codTipoMovimento: '2.1.10',
      localEstoque: '001',
      serie: 'PVP',
      integraAplicacao: 'T',
      tipo: 'P',
      status: 'A',
      codigoUsuario: usuario.trim().toLowerCase(),
      codcoligadacfo: 0,
      codCondPagamento: condicaoPagamento,
      codcoligadacfoaux: 0,
      codcfoaux: 'CXXXXXXXXXX',
      usuarioCriacao: usuario.trim().toLowerCase(),
      prazoEntrega: 0,
      codRepresentante: representante,
      ...data,
    });

    return res.json(info);
  }

  async addItemMovimento(req, res) {
    const {
      id,
      idMov,
      preco,
      quantidade,
      observacao,
      dataEmissao,
      dataEntrega,
      unidadeControle,
      codtb2fat,
      codtb3fat,
      codtb4fat,
    } = req.body;

    let nSeq = await MovimentoItem.max('nseqitmmov', {
      where: { idMov },
    });

    if (Number.isNaN(nSeq)) {
      nSeq = 1;
    } else {
      nSeq += 1;
    }
    const itens = await MovimentoItem.create({
      nseqitmmov: nSeq,
      idprd: id,
      id: idMov,
      coligada: 1,
      precoUnitario: preco,
      valorTotalItem: quantidade * preco,
      codigoUnidade: unidadeControle,
      quantidade: Number(quantidade),
      observacao,
      numeroSequencial: nSeq,
      codloc: '001',
      filial: 1,
      dataEmissao,
      dataEntrega,
      unidadeControle,
      quantidadeOriginal: Number(quantidade),
      quantidadeTotal: Number(quantidade),
      quantidadeReceber: Number(quantidade),
      codtb2fat,
      codtb3fat,
      codtb4fat,
      campoLivre: observacao,
    });
    return res.json(itens);
  }

  async index(req, res) {
    const { codigo, data, usuario, tipoMovimento } = req.query;

    const movimento = await Movimento.findAll({
      include: [
        {
          model: Cliente,
          as: 'cliente',
          where: {
            codcfo: {
              [Op.like]: `${codigo}%`,
            },
          },
          attributes: ['nome', 'cgccfo', 'campoLivre'],
        },
      ],

      where: {
        // dataEmissao: req.query.dataEmissao,
        usuarioCriacao: usuario,
        dataEmissao: data,
        codTipoMovimento: tipoMovimento,
        status: tipoMovimento === '2.1.10' ? 'A' : 'F',
        // romaneio: romaneio || null,
      },
    });
    return res.json(movimento);
  }

  async buscaPorCliente(req, res) {
    const { codcfo } = req.query;
    const cliente = await Cliente.findOne({
      tableHint: 'NOLOCK',
      include: [
        {
          model: ClienteComplementar,
          as: 'complementar',
          tableHint: 'NOLOCK',
        },
        {
          model: ClienteDefaults,
          as: 'defaults',
          tableHint: 'NOLOCK',
        },
        {
          model: CondicaoPagamento,
          as: 'condicaoPagamento',
          tableHint: 'NOLOCK',
        },
      ],
      where: {
        codcfo,
      },
    });

    if (!cliente) {
      return res.status(202).json({ message: 'Cliente não encontrado' });
    }

    if (cliente.bloqueado === 1) {
      return res.status(202).json({
        message: `${cliente.codcfo} - ${cliente.nome} BLOQUEADO.\n\nMOTIVO: ${cliente.complementar.motivoBloqueio}`,
      });
    }
    if (cliente.ativo === 0) {
      return res.status(202).json({
        message: 'Cliente Inativo',
      });
    }
    let abertoFinanceiro = 0;
    if (cliente.valorop3 === 0) {
      const financeiro = await Financeiro.sum('valorOriginal', {
        where: {
          codcfo,
          status: '0',
          pagrec: { [Op.in]: [1, 3] },
          vencimento: {
            [Op.lte]: subDays(new Date(), 1),
          },
        },
      });
      abertoFinanceiro = financeiro;
    } else {
      const financeiro = await FinanceiroFilial.sequelize.query(
        `SELECT
        SUM(F.VALORORIGINAL) AS valorAberto
      FROM
        [FLAN] F (NOLOCK)
      WHERE
          F.CODCFO = '${codcfo}'
          AND F.STATUSLAN = 0
          AND F.PAGREC IN ('1','3')
          AND F.DATAVENCIMENTO <= '${format(
            subDays(new Date(), 1),
            'yyyy-MM-dd'
          )}'
          `
      );
      const { valorAberto } = financeiro[0][0];
      abertoFinanceiro = Number(valorAberto);
    }

    let saldo = cliente.complementar.limite;
    saldo = cliente.complementar.limite - abertoFinanceiro;
    return res.json({ cliente, saldo });
  }

  async buscaMovimentoPorDataRomaneio(req, res) {
    const { romaneio, dataEmissao } = req.query;

    const [movimentos] = await Movimento.sequelize.query(`
          SELECT
              T.IDMOV AS id,
              T.CODTB5FAT AS romaneio,
              T.DATAEMISSAO as dataEmissao,
              F.CODCFO as codcfo,
              F.RUA as rua,
              F.NUMERO as numero,
              F.BAIRRO as bairro,
              F.CIDADE as cidade,
              COMPL.PRIORIDADE as prioridade,
              COMPL.LATITUDE as latitude,
              COMPL.LONGITUDE as longitude
          FROM
              TMOV T (NOLOCK)
              INNER JOIN FCFO F (NOLOCK)
                  ON T.CODCFO = F.CODCFO
              INNER JOIN FCFOCOMPL COMPL (NOLOCK)
                  ON F.CODCFO = COMPL.CODCFO
          WHERE
              T.CODTB5FAT = '${romaneio}'
              AND T.DATAEMISSAO = '${dataEmissao}'
              AND T.CODTMV = '2.1.10'
              AND COMPL.LATITUDE IS NOT NULL
    `);

    // const movimentos = rawQuery[0];

    // const movimentos = await Movimento.findAll({
    //   where: {
    //     romaneio,
    //     dataEmissao,
    //     codTipoMovimento: '2.1.10',
    //     status: {
    //       [Op.ne]: 'C',
    //     },
    //   },
    //   attributes: ['idmov', 'romaneio', 'dataEmissao'],
    //   include: [
    //     {
    //       model: Cliente,
    //       as: 'cliente',
    //       attributes: ['codcfo', 'rua', 'numero', 'bairro', 'cidade'],
    //       include: [
    //         {
    //           model: ClienteComplementar,
    //           as: 'complementar',
    //           attributes: ['prioridade', 'latitude', 'longitude'],
    //           where: {
    //             latitude: { [Op.ne]: null },
    //           },
    //         },
    //       ],
    //     },
    //   ],
    // });
    return res.json(movimentos);
  }

  async getPesoAproximadoCaminhao(req, res) {
    const { data, usuario } = req.query;
    console.log(data, usuario);
    const pesos = await Movimento.sequelize.query(`
      SELECT
        T.CODTB5FAT AS romaneio,
        SUM(I.QUANTIDADE * P.PESOLIQUIDO * 1.12) AS pesoCaminhao
      FROM
        TMOV T,
        FCFOCOMPL COMPL,
        TITMMOV I,
        TPRODUTO P
      WHERE
        I.IDPRD = P.IDPRD
        AND T.CODCFO = COMPL.CODCFO
        AND T.IDMOV = I.IDMOV
        AND T.CODTMV = '2.1.10'
        AND T.STATUS <> 'C'
        AND T.DATAEMISSAO = '${data}'
        AND T.USUARIOCRIACAO = '${usuario}'
      GROUP BY
        T.CODTB5FAT
    `);
    return res.json(pesos[0]);
  }

  async buscaMovimentosEntrada(req, res) {
    const { dataEmissao } = req.query;

    const movimentos = await MovimentoFilial.sequelize.query(`
        SELECT
            I.IDMOV as id,
            T.NUMEROMOV as numeroMovimento,
            P.IDPRD as idPrd,
            P.CODIGOAUXILIAR as codigoAuxiliar,
            P.NOMEFANTASIA as nome,
            T.DATACRIACAO as dataCriacao,
            T.DATAEXTRA1 AS dataAbate,
            T.DATAFECHAMENTO as dataFechamento
        FROM
            TMOV T (NOLOCK)
        LEFT JOIN TITMMOV I (NOLOCK)
            ON T.CODCOLIGADA = I.CODCOLIGADA AND T.IDMOV = I.IDMOV
        LEFT JOIN TPRODUTO P (NOLOCK)
            ON I.IDPRD = P.IDPRD
        WHERE
            T.CODTMV = '1.2.15'
            AND T.STATUS != 'C'
            AND T.DATAEXTRA1 = '${dataEmissao}'`);
    return res.json(movimentos[0]);
  }

  async editaMovimentoEntrada(req, res) {
    const { id, idPrd } = req.params;

    const movimento = await MovimentoFilial.findOne({
      include: [
        {
          model: ClienteFilial,
          as: 'cliente',
          attributes: ['codcfo', 'nome', 'cidade'],
        },
      ],
      where: {
        id,
      },
    });

    const item = await MovimentoItemFilial.findOne({
      include: [
        {
          model: Produto,
          as: 'produto',
        },
      ],
      where: {
        idPrd,
        id,
      },
    });
    return res.json({ movimento, item });
  }

  async show(req, res) {
    const { id } = req.params;
    const movimento = await Movimento.findOne({
      include: ['cliente'],
      where: {
        id,
      },
    });
    const items = await MovimentoItem.findAll({
      include: ['produto'],
      where: {
        id,
      },
    });
    return res.json({ movimento, items });
  }

  async deleteMov(req, res) {
    const { id } = req.params;
    const movimento = await Movimento.findOne({
      where: {
        id,
      },
    });
    if (!movimento) {
      return res.json({ message: 'Movimento não encontrado' });
    }
    await MovimentoItem.destroy({
      where: {
        id,
      },
    });
    await Movimento.destroy({
      where: {
        id,
      },
    });
    return res.json({ message: `Movimento ${id} excluído com sucesso` });
  }

  async deleteItem(req, res) {
    const { idMov, nseqitmmov } = req.params;

    await MovimentoItem.destroy({
      where: {
        id: idMov,
        nseqitmmov,
      },
    });

    return res.json({ message: `Item excluído com sucesso` });
  }

  async novoPedido(req, res) {
    const id = await AutoIncremento.findOne({
      attributes: ['valAutoInc'],
      tableHint: 'NOLOCK',
      where: {
        codSistema: 'T',
        codAutoInc: 'IDMOV',
        codColigada: '1',
      },
      limit: 0,
    });

    const numeroMov = await AutoIncremento.findOne({
      attributes: ['valAutoInc'],
      tableHint: 'NOLOCK',
      where: {
        codSistema: 'T',
        codAutoInc: 'PVP000000',
        codColigada: '1',
      },
      limit: 0,
    });

    console.log(
      `Numero do Movimento: ${numeroMov.valAutoInc} Id do Movimento: ${id.valAutoInc}`
    );

    const novoId = id.valAutoInc + 1;
    const novoNumeroMovimento = numeroMov.valAutoInc + 1;

    await AutoIncremento.sequelize.query(
      `UPDATE GAUTOINC SET VALAUTOINC = ${novoId}
    WHERE
      CODAUTOINC = 'IDMOV'
      AND CODCOLIGADA = 1
      AND CODSISTEMA = 'T'`
    );
    await AutoIncremento.sequelize.query(
      `UPDATE GAUTOINC SET VALAUTOINC = ${novoNumeroMovimento}
    WHERE
      CODAUTOINC = 'PVP000000'
      AND CODCOLIGADA = 1
      AND CODSISTEMA = 'T'`
    );

    return res.json({ id: novoId, numeroMov: novoNumeroMovimento });
  }

  async addRomaneioPedido(req, res) {
    const { selectedMovimentos, romaneio } = req.body;

    await Movimento.sequelize.query(
      `UPDATE TMOV SET CODTB5FAT = '${romaneio}' WHERE IDMOV IN (${selectedMovimentos})`
    );
    return res.json({ message: 'Romaneio adicionado com sucesso!' });
  }

  async gerarRelatorioPDF(req, res) {
    const { dataEmissao, usuario } = req.query;

    const movimento = await Movimento.sequelize.query(
      findMovimentosToDataUsuario(dataEmissao, usuario)
    );

    const mov = movimento[0].map(item => ({
      id: item.id,
      // dataEmissao: format(addHours(item.dataEmissao, 3), 'dd/MM/yyyy'),
      codcfo: item.codcfo,
      codigoAuxiliar: item.codigoAuxiliar,
      descricaoProduto: item.produto,
      quantidade: item.quantidade,
      unidade: item.unidade,
      preco: new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
      }).format(item.precoUnitario),
    }));

    const filePDF = path.join(
      __dirname,
      '..',
      '..',
      '..',
      'reports',
      `${usuario}_pedidos.pdf`
    );

    try {
      const compile = async (templateName, data) => {
        const filePath = fs
          .readFileSync(
            path.join(
              __dirname,
              '..',
              '..',
              '..',
              'src',
              'templates',
              `${templateName}.hbs`
            )
          )
          .toString('utf8');

        if (!filePath) {
          throw new Error(`Could not find ${templateName}.hbs in generatePDF`);
        }

        const template = handlebars.compile(filePath)(data);

        return template;
      };

      const browser = await puppeteer.launch({ headless: true });
      const page = await browser.newPage();
      const dataFormatada = moment(dataEmissao).format('DD/MM/YYYY');
      const content = await compile('movimentos', { mov, dataFormatada });

      await page.setContent(content);
      const pdf = await page.pdf({
        path: filePDF,
        format: 'A4',
        printBackground: true,
        landscape: false,
        margin: {
          left: '0.5cm',
          top: '1cm',
          right: '0.5cm',
          bottom: '1cm',
        },
        scale: 1,
      });

      await browser.close();
      pdf.toString('base64');
      return res.status(200).json({ message: 'Relatório gerado com sucesso' });
    } catch (error) {
      console.log('Erro PDF: ', error);
      return new Error('Error', error);
    }
  }
}

export default new MovimentoController();
