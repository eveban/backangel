import MovimentoItemFilial from '../models/filial/movimento/MovimentoItem';
import MovimentoFilial from '../models/filial/movimento/Movimento';
import SaldoProduto from '../models/filial/produto/SaldoProduto';

class ItemController {
  async updateItemMovimento(req, res) {
    const {
      valorArroba,
      percentualRetalho,
      fator,
      fatorRetalho,
      pesoBruto,
      pesoRetalho,
      pesoLiquido,
    } = req.body;

    const { idMov, numeroSequencialItem } = req.params;

    const item = await MovimentoItemFilial.sequelize.query(`
				UPDATE COMPL SET
						COMPL.VALORARROBA = ${valorArroba.replace(',', '.')},
						COMPL.PERCRET = ${percentualRetalho ? percentualRetalho.replace(',', '.') : 0},
						COMPL.FATOR = ${fator},
						COMPL.FATRET = ${fatorRetalho},
						COMPL.PESBRU = ${pesoBruto},
						COMPL.PESRET = ${pesoRetalho},
						COMPL.PESLIQ = ${pesoLiquido}
				FROM
						TMOV T (NOLOCK)
						INNER JOIN TITMMOVCOMPL COMPL (NOLOCK)
								ON T.IDMOV = COMPL.IDMOV
				WHERE
						COMPL.IDMOV = ${idMov}
						AND COMPL.NSEQITMMOV = ${numeroSequencialItem}

				UPDATE T SET
					T.DATAFECHAMENTO = GETDATE()
				FROM
						TMOV T (NOLOCK)
				WHERE
						T.IDMOV = ${idMov}
		`);
    return res.json(item);
  }

  // async fechamentoItems(req, res) {
  //   const { dataCriacao } = req.query;

  //   const items = await MovimentoFilial.sequelize.query(`
  //     SELECT T.DATACRIACAO as dataCriacao,
  //         I.IDPRD AS idProduto,
  //         P.CODIGOAUXILIAR AS codigoAuxiliar,
  //         P.DESCRICAO AS descricao,
  //         Sum(COMPL.PESBRU) AS pesoBruto,
  //         Sum(COMPL.PESRET) AS pesoRetalho,
  //         Sum(COMPL.PESLIQ) AS pesoLiquido,
  //         Sum(I.QUANTIDADE) AS quantidade
  //     FROM
  //         TMOV T (NOLOCK),
  //         TITMMOV I (NOLOCK),
  //         TITMMOVCOMPL COMPL (NOLOCK),
  //         TPRODUTO P (NOLOCK)
  //     WHERE
  //         T.DATACRIACAO = '${dataCriacao}'
  //         AND T.IDMOV = I.IDMOV
  //         AND T.IDMOV = COMPL.IDMOV
  //         AND COMPL.IDMOV = I.IDMOV
  //         AND COMPL.NSEQITMMOV = I.NSEQITMMOV
  //         AND P.IDPRD = I.IDPRD
  //         AND T.CODTMV = '1.2.15'
  //     GROUP  BY
  //         T.DATACRIACAO,
  //         I.IDPRD,
  //         P.CODIGOAUXILIAR,
  //         P.DESCRICAO`);

  //   return res.json(items[0]);
  // }

  async findItemsMovDateSlaughter(req, res) {
    const { dataCriacao } = req.query;

    const items = await MovimentoFilial.sequelize.query(`
      SELECT
          --T.DATACRIACAO as dataCriacao,
          I.IDPRD AS idProduto,
          P.CODIGOAUXILIAR AS codigoAuxiliar,
          P.DESCRICAO AS descricao,
          T.DATAEXTRA1 AS dataAbate
      FROM
          TMOV T (NOLOCK),
          TITMMOV I (NOLOCK),
          TPRODUTO P (NOLOCK)
      WHERE
          T.DATAEXTRA1 = '${dataCriacao}'
          AND T.IDMOV = I.IDMOV
          AND P.IDPRD = I.IDPRD
          AND T.CODTMV = '1.2.15'
      GROUP BY
          --T.DATACRIACAO,
          T.DATAEXTRA1,
          I.IDPRD,
          P.CODIGOAUXILIAR,
          P.DESCRICAO`);

    return res.json(items[0]);
  }

  async findPesos(req, res) {
    const { idProduto, dataCriacao } = req.query;

    const [data] = await MovimentoItemFilial.sequelize.query(`
        SELECT
        Sum(COMPL.PESBRU) AS pesoBruto,
        Sum(COMPL.PESRET) AS pesoRetalho,
        Sum(COMPL.PESLIQ) AS pesoLiquido,
        Sum(I.QUANTIDADE) AS quantidade
      FROM
        TMOV T (NOLOCK),
        TITMMOV I (NOLOCK),
        TITMMOVCOMPL COMPL (NOLOCK)
      WHERE
        T.IDMOV = I.IDMOV
        AND T.IDMOV = COMPL.IDMOV
        AND COMPL.IDMOV = I.IDMOV
        AND COMPL.NSEQITMMOV = I.NSEQITMMOV
        AND T.CODTMV = '1.2.15'
        AND T.DATAEXTRA1 = '${dataCriacao.substring(0, 10)}'
        AND I.IDPRD = ${idProduto}
  `);

    return res.json(data[0]);
  }

  async updateEstoque(req, res) {
    const item = req.body;
    if (item[0].idPai !== 5334) {
      await item.map(e =>
        SaldoProduto.sequelize.query(`UPDATE TPRDLOC SET
            SALDOFISICO2 = SALDOFISICO2 + ${e.peso
              .replace('.', '')
              .replace(',', '.')},
            SALDOFISICO10 = SALDOFISICO10 + ${e.quantidade}
        WHERE
            IDPRD = ${e.idFilho}
            AND CODLOC = '001'`)
      );
    } else {
      await item.map(e =>
        SaldoProduto.sequelize.query(`UPDATE TPRDLOC SET
            SALDOFISICO2 = SALDOFISICO2 + ${
              Number(e.peso.replace('.', '').replace(',', '.')) > 0
                ? e.peso.replace('.', '').replace(',', '.')
                : e.quantidade
            },
            SALDOFISICO10 = SALDOFISICO10 + ${e.quantidade}
        WHERE
            IDPRD = ${e.idFilho}
            AND CODLOC = '001'`)
      );
    }

    return res.json({ message: 'Dados incluídos com sucesso' });
  }
}

export default new ItemController();
