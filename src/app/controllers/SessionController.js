import jwt from 'jsonwebtoken';
import authConfig from '../../config/auth';
import Usuario from '../models/Usuario';

class SessionController {
  async store(req, res) {
    const { username, senha } = req.body;
    const user = await Usuario.findOne({
      where: { username },
      limit: 0,
    });
    if (!user) {
      return res.status(401).json({ error: 'Dados inválidos' });
    }
    if (!(await user.verificaSenha(senha))) {
      return res.status(401).json({ error: 'Dados inválidos' });
    }

    const { id, nome, email } = user;

    return res.json({
      user: {
        id,
        username,
        nome,
        email,
      },
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();
