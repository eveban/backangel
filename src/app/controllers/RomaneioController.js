import Romaneio from '../models/movimento/Romaneio/Romaneio';

class RomaneioController {
  async index(req, res) {
    const romaneio = await Romaneio.findAll();
    return res.json(romaneio);
  }
}

export default new RomaneioController();
