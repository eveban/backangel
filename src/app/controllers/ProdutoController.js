import { Op } from 'sequelize';
import Produto from '../models/produto/Produto';
import ProdutoFilial from '../models/filial/produto/Produto';
import ProdutoDef from '../models/produto/ProdutoDef';
import TabelaPreco from '../models/produto/TabelaPreco/TabelaPreco';

import ProdutoComposto from '../models/filial/produto/ProdutoComposto';

class ProdutoController {
  async index(req, res) {
    const { dataVigencia, descricao } = req.query;
    const produto = await Produto.findAll({
      tableHint: 'NOLOCK',
      attributes: ['codigoAuxiliar', 'fantasia', 'descAuxiliar'],
      include: [
        {
          model: TabelaPreco,
          tableHint: 'NOLOCK',
          as: 'items',
          where: {
            preco: {
              [Op.gt]: 0,
            },
            dataVigencia: {
              [Op.gte]: dataVigencia,
            },
          },
        },
      ],
      where: {
        descricao: {
          [Op.like]: `%${descricao}%`,
        },
        inativo: 0,
      },
    });
    return res.json(produto);
  }

  async show(req, res) {
    const { codigoAuxiliar } = req.query;
    const produto = await Produto.findOne({
      tableHint: 'NOLOCK',
      attributes: [
        'idprd',
        'codigoprd',
        'codigoAuxiliar',
        'fantasia',
        'descAuxiliar',
        'pesoLiquido',
      ],
      include: [
        {
          model: ProdutoDef,
          as: 'defaults',
          tableHint: 'NOLOCK',
          attributes: [
            'preco1',
            'unidadeControle',
            'codtb2fat',
            'codtb3fat',
            'codtb4fat',
          ],
        },
      ],
      where: {
        codigoAuxiliar,
      },
      limit: 0,
    });
    if (!produto) {
      return res.status(202).json({ message: 'Produto não encontrado' });
    }
    return res.json(produto);
  }

  async findProdutoComposto(req, res) {
    const { idProduto } = req.query;
    const produtosCompostos = await ProdutoFilial.findAll({
      attributes: ['id', 'descricao'],
      include: [
        {
          model: ProdutoComposto,
          as: 'composto',
          where: { id: idProduto },
        },
      ],
    });
    return res.json(produtosCompostos);
  }
}

export default new ProdutoController();
