// eslint-disable-next-line import/no-extraneous-dependencies
import moment from 'moment';
import FechamentoNota from '../models/filial/movimento/entrada/FechamentoNota';
import FechamentoItem from '../models/filial/movimento/entrada/FechamentoItem';

class FechamentoNotaController {
  async store(req, res) {
    const {
      idProduto,
      dataCriacao,
      codigoAuxiliar,
      quantidade,
      pesoBruto,
      pesoRetalho,
      pesoLiquido,
      percTraseiro,
      percDianteiro,
      percPA,
      items,
    } = req.body;

    const fechamento = await FechamentoNota.create({
      idPrd: idProduto,
      dataAbate: moment(dataCriacao, 'YYYY-MM-DD').format('YYYY-MM-DD'),
      dataFechamento: moment(new Date(), 'YYYY-MM-DD').format(
        'YYYY-MM-DD HH:mm:ss'
      ),
      codigoAuxiliar,
      quantidade,
      pesoBruto,
      pesoRetalho,
      pesoLiquido,
      fatorTraseiro: percTraseiro.replace(',', '.'),
      fatorDianteiro: percDianteiro.replace(',', '.'),
      fatorPontaAgulha: percPA,
    });
    const { id } = fechamento;
    await items.map(item => {
      FechamentoItem.create({
        idFechamento: id,
        idPrdPai: item.idPai,
        idPrd: item.idFilho,
        peso: item.peso.replace('.', '').replace(',', '.'),
        quantidade: item.quantidade,
      });
      return item;
    });

    return res.status(201).json({ message: 'Salvo com sucesso' });
  }

  async index(req, res) {
    const fechamento = await FechamentoNota.findAll({
      include: [
        {
          model: FechamentoItem,
          as: 'itens',
        },
      ],
    });
    return res.json(fechamento);
  }
}
export default new FechamentoNotaController();
