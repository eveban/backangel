import ProdutoFilial from '../models/filial/produto/Produto';
import ProdutoComposto from '../models/filial/produto/ProdutoComposto';
import ProdutoComplementar from '../models/filial/produto/ProdutoComplementar';

class DesossaController {
  async getProdutoCompostoPorId(req, res) {
    const { id } = req.params;

    console.log(id);
    const produtosCompostos = await ProdutoFilial.findAll({
      attributes: ['id', 'descricao', 'codigoAuxiliar', 'pesoMedio'],
      include: [
        {
          model: ProdutoComposto,
          as: 'composto',
          where: { id },
        },
        {
          model: ProdutoComplementar,
          as: 'complementar',
        },
      ],
      order: ['descricao'],
    });
    return res.json(produtosCompostos);
  }
}

export default new DesossaController();
