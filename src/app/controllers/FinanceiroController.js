import sequelize, { Op, QueryTypes } from 'sequelize';

import { format, addDays } from 'date-fns';

import Financeiro from '../models/financeiro/Financeiro';
import FinanceiroFilial from '../models/filial/financeiro/Financeiro';
// import Cliente from '../models/cliente/Cliente';
// import ClienteComplementar from '../models/cliente/ClienteComplementar';

class FinanceiroController {
  async index(req, res) {
    const financeiro = await Financeiro.findOne({
      where: {
        codcfo: '01.0000',
        status: '0',
        pagrec: { [Op.in]: [1] },
      },
      attributes: [
        'valorOriginal',
        [sequelize.fn('sum', sequelize.col('valorOriginal')), 'valorAberto'],
      ],
      limit: 0,
      group: ['valorOriginal'],
    });
    return res.json(financeiro);
  }

  async titulosAbertoPorRepresentante(req, res) {
    const dayOfWeek = new Date().getDay();
    let past;

    if (dayOfWeek === 1) {
      past = format(addDays(new Date(), -3), 'yyyy-MM-dd', {
        timeZone: 'America/Sao_Paulo',
      });
    } else {
      past = format(addDays(new Date(), -1), 'yyyy-MM-dd', {
        timeZone: 'America/Sao_Paulo',
      });
    }
    const { usuario, cliente } = req.query;

    const financeiro = await FinanceiroFilial.sequelize.query(
      `SELECT
          F.IDLAN,
          F.NUMERODOCUMENTO,
          F.DATAEMISSAO,
          F.DATAVENCIMENTO,
          F.CODCFO,
          C.NOMEFANTASIA,
          C.TELEX,
          DATEDIFF(DAY,F.DATAVENCIMENTO,GETDATE()) AS DIAS,
          F.VALORORIGINAL,
          ((F.VALORORIGINAL * 4 / 100) / 30) * DATEDIFF(DAY,F.DATAVENCIMENTO, GETDATE()) AS ACRESCIMO,
          SUM(F.VALORORIGINAL + ((F.VALORORIGINAL * 4 / 30) / 100) * DATEDIFF(DAY,F.DATAVENCIMENTO, GETDATE())) AS LIQUIDO
      FROM
          [FLAN] F (NOLOCK),
          [FCFO] C (NOLOCK),
          [FCFOCOMPL] COMPL (NOLOCK)
      WHERE   F.CODCFO = C.CODCFO
          AND F.CODCOLCFO = C.CODCOLIGADA

          AND C.CODCFO = COMPL.CODCFO
          AND C.CODCOLIGADA = COMPL.CODCOLIGADA

          AND F.CODCOLCFO = COMPL.CODCOLIGADA
          AND F.CODCFO = COMPL.CODCFO

          AND F.CODCOLIGADA = 1
          AND F.CODFILIAL = 1
          AND F.STATUSLAN IN (0)
          AND F.PAGREC = 1
          AND F.NFOUDUP != 1
          AND C.CAMPOALFAOP2 != '10'
          AND F.CODRPR != '010'
          AND F.CODCXA != 'CAIXA'
          AND F.CODCFO LIKE '%${cliente}%'
          AND F.DATAVENCIMENTO <= '${past}'
          AND COMPL.VENDEDOR = '${usuario}'

      GROUP BY
          F.CODCFO,
          F.IDLAN,
          F.NUMERODOCUMENTO,
          F.DATAEMISSAO,
          F.DATAVENCIMENTO,
          C.NOMEFANTASIA,
          C.TELEX,
          F.DATAVENCIMENTO,
          F.VALORORIGINAL
      ORDER BY
          F.CODCFO,
          F.DATAVENCIMENTO DESC`,
      { nest: true, type: QueryTypes.SELECT }
    );

    return res.json(financeiro);
  }

  // const financeiro = await Financeiro.findAll({
  //   include: [
  //     {
  //       model: Cliente,
  //       as: 'cliente',
  //       include: [
  //         {
  //           model: ClienteComplementar,
  //           as: 'complementar',
  //         },
  //       ],
  //       attributes: ['nome', 'cgccfo', 'campoalfaop2'],
  //       where: {
  //         campoalfaop2: {
  //           [Op.ne]: '10',
  //         },
  //       },
  //     },
  //   ],
  //   attributes: [
  //     'codcfo',
  //     'numerodocumento',
  //     'valorOriginal',
  //     // [Date(`${new Date()} - vencimento`), 'dias_em_atraso'],
  //   ],
  //   where: {
  //     status: { [Op.in]: [0] },
  //     pagrec: { [Op.in]: [1] },
  //     nfoudup: { [Op.ne]: [1] },
  //     vencimento: { [Op.lte]: ontem },
  //     codcxa: { [Op.notLike]: 'CAIXA' },
  //     codrpr: { [Op.in]: ['006', '024', '026'] },
  //   },

  //   order: [['codcfo'], ['vencimento', 'DESC']],
  //   // group: ['valorOriginal'],
  // });
  // return res.json(financeiro);
}

export default new FinanceiroController();
