import * as Yup from 'yup';

import ClienteComplementar from '../models/cliente/ClienteComplementar';

class RotaController {
  async update(req, res) {
    const { codcfo, lat, lng } = req.body;

    const schema = Yup.object().shape({
      codcfo: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const cliente = await ClienteComplementar.findOne({
      where: { codcfo },
    });

    if (cliente.latitude || cliente.longitude) {
      return res.json({
        message: 'As coordenadas do cliente já foram cadastradas!',
      });
    }

    await ClienteComplementar.update(
      {
        latitude: lat,
        longitude: lng,
      },
      {
        where: { codcfo },
      }
    );

    return res.status(200).json(cliente);
  }
}

export default new RotaController();
