import * as Yup from 'yup';
import Usuario from '../models/Usuario';

class UsuarioController {
  async store(req, res) {
    const schema = Yup.object().shape({
      nome: Yup.string().required(),
      username: Yup.string().required('Nome de usuário obrigatório'),
      email: Yup.string().email('Formato inválido'),
      senha: Yup.string()
        .required()
        .min(6),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Falha da validação dos dados' });
    }

    const maxId = await Usuario.max('id');
    const novoId = maxId + 1;

    const userExists = await Usuario.findOne({
      where: { username: req.body.username },
    });
    if (userExists) {
      return res
        .status(400)
        .json({ error: `O Usuário ${req.body.username} já existe` });
    }

    const { id, nome, email } = await Usuario.create({
      ...req.body,
      id: novoId,
    });
    return res.json({ id, nome, email });
  }

  async index(req, res) {
    const usuario = await Usuario.findAll({
      tableHint: 'NOLOCK',
    });
    return res.json(usuario);
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      nome: Yup.string(),
      username: Yup.string().required('Nome de usuário obrigatório'),
      email: Yup.string().email('Formato inválido'),
      senhaAnterior: Yup.string().min(6),
      senha: Yup.string()
        .min(6)
        .when('senhaAnterior', (senhaAnterior, field) =>
          senhaAnterior ? field.required() : field
        ),
      confirmaSenha: Yup.string().when('senha', (senha, field) =>
        senha ? field.required().oneOf([Yup.ref('senha')]) : field
      ),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Falha da validação dos dados' });
    }

    const { username, senhaAnterior } = req.body;

    const usuario = await Usuario.findByPk(req.userId);
    if (username && username !== usuario.username) {
      const userExists = await Usuario.findOne({
        where: { username },
      });
      if (userExists) {
        return res
          .status(400)
          .json({ error: `O Usuário ${req.body.username} já existe` });
      }
    }
    if (senhaAnterior && !(await usuario.verificaSenha(senhaAnterior))) {
      return res.status(401).json({ error: 'password not match' });
    }
    const { id, nome, email } = await usuario.update(req.body);
    return res.json({ id, username, nome, email });
  }
}

export default new UsuarioController();
