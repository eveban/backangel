import { Op } from 'sequelize';
import { subDays, format } from 'date-fns';

import Cliente from '../models/cliente/Cliente';
import ClienteComplementar from '../models/cliente/ClienteComplementar';
import ClienteDefaults from '../models/cliente/ClienteDefaults';
import FinanceiroFilial from '../models/filial/financeiro/Financeiro';
import Financeiro from '../models/financeiro/Financeiro';

class ClienteController {
  async index(req, res) {
    const clientes = await Cliente.findAll({
      where: {
        pagrec: { [Op.in]: [1, 3] },
      },
      attributes: ['nome', 'cgccfo', 'ativo', 'bloqueado'],
      include: [
        {
          model: ClienteComplementar,
          as: 'complementar',
        },
        {
          model: ClienteDefaults,
          as: 'defaults',
          /*    where: {
            condicaoPagamento: { [Op.ne]: null },
          }, */
        },
      ],
    });
    return res.json(clientes);
  }

  async buscaClientePorPraca(req, res) {
    const clientes = await Cliente.findAll({
      where: {
        codcfo: {
          [Op.like]: `${req.query.codigo}%`,
        },
        rua: {
          [Op.ne]: null,
        },
        ativo: 1,
      },
      attributes: ['codcfo', 'rua', 'numero', 'bairro', 'cidade'],
      include: [
        {
          model: ClienteComplementar,
          as: 'complementar',
          attributes: ['latitude', 'longitude'],
          where: {
            latitude: {
              [Op.ne]: null,
            },
          },
        },
      ],
    });
    return res.json(clientes);
  }

  async buscaPorClientePorCodigo(req, res) {
    const { codcfo } = req.query;
    const cliente = await Cliente.findOne({
      tableHint: 'NOLOCK',
      include: [
        {
          model: ClienteComplementar,
          as: 'complementar',
          tableHint: 'NOLOCK',
        },
        //   {
        //     model: ClienteDefaults,
        //     as: 'defaults',
        //     tableHint: 'NOLOCK',
        //   },
      ],
      where: {
        codcfo,
      },
    });

    if (!cliente) {
      return res.status(202).json({ message: 'Cliente não encontrado' });
    }

    const [condicaoPagamento] = await Cliente.sequelize.query(
      `SELECT
        CP.NOME as descricao
      FROM
        TCPGFCFO CLIENTE,
        TCPG CP
      WHERE
        CP.CODCOLIGADA = CLIENTE.CODCOLIGADA
        AND CP.CODCPG = CLIENTE.CODCPGVENDA
        AND CLIENTE.CODCFO = '${codcfo}'`
    );

    let abertoFinanceiro = 0;
    if (cliente.valorop3 === 0) {
      const financeiro = await Financeiro.sum('valorOriginal', {
        where: {
          codcfo,
          status: '0',
          pagrec: { [Op.in]: [1, 3] },
          vencimento: {
            [Op.lte]: subDays(new Date(), 1),
          },
        },
      });
      abertoFinanceiro = financeiro;
    } else {
      const financeiro = await FinanceiroFilial.sequelize.query(
        `SELECT
          SUM(F.VALORORIGINAL) AS valorAberto
        FROM
          [FLAN] F (NOLOCK)
        WHERE
          F.CODCFO = '${codcfo}'
          AND F.STATUSLAN = 0
          AND F.PAGREC IN ('1','3')
          AND F.DATAVENCIMENTO <= '${format(
            subDays(new Date(), 1),
            'yyyy-MM-dd'
          )}'
          `
      );
      const { valorAberto } = financeiro[0][0];
      abertoFinanceiro = Number(valorAberto);
    }

    let saldo = cliente.complementar.limite;
    saldo = cliente.complementar.limite - abertoFinanceiro;

    return res.json({ cliente, saldo, condicaoPagamento });
  }
}

export default new ClienteController();
