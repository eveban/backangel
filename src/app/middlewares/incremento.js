import AutoIncremento from '../models/parametros/AutoIncremento';

export default async (req, res, next) => {
  const autoIncremento = await AutoIncremento.findOne({
    attributes: ['valAutoInc'],
    tableHint: 'NOLOCK',
    where: {
      codSistema: 'T',
      codAutoInc: 'IDMOV',
      codColigada: '1',
    },
    limit: 0,
  });

  const novoNumero = autoIncremento.valAutoInc + 1;
  try {
    await AutoIncremento.sequelize.query(
      `UPDATE
        GAUTOINC SET VALAUTOINC = ${novoNumero}
      WHERE
        CODAUTOINC = 'IDMOV'
        AND CODCOLIGADA = 1
        AND CODSISTEMA = 'T'`
    );
    return next();
  } catch (err) {
    return res.status(401).json({ error: 'Erro ao retornar valor' });
  }
};
