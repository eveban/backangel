module.exports = {
  dialect: 'mssql',
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  // logging: false,
  define: {
    timestamps: false,
  },
  dialectOptions: {
    options: {
      tdsVersion: '7_3_B',
      encrypt: false,
      enableArithAbort: false,
      requestTimeout: 300000,
      cryptoCredentialsDetails: {
        minVersion: 'TLSv1',
      },
    },
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
