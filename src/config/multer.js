import multer from 'multer';
import crypto from 'crypto';
import { extname, resolve } from 'path';

/**
 * destination: Local que será enviado os arquvivos
 * filename: Nome do arquivo, será alterado para manter um nome aleatório mantendo a extensão,
 * para não haver conflito com 2 nome iguais
 */
export default {
  storage: multer.diskStorage({
    destination: resolve(__dirname, '..', '..', 'tmp', 'uploads'),
    filename: (req, file, cb) => {
      crypto.randomBytes(8, (err, res) => {
        if (err) return cb(err);
        return cb(null, res.toString('hex') + extname(file.originalname));
      });
    },
  }),
};
