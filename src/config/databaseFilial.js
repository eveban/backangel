module.exports = {
  dialect: 'mssql',
  host: process.env.DB_HOST_F1,
  port: process.env.DB_PORT_F1,
  username: process.env.DB_USER_F1,
  password: process.env.DB_PASS_F1,
  database: process.env.DB_NAME_F1,
  // logging: false,
  define: {
    timestamps: false,
  },
  dialectOptions: {
    options: {
      tdsVersion: '7_3_B',
      encrypt: false,
      enableArithAbort: false,
      requestTimeout: 300000,
      cryptoCredentialsDetails: {
        minVersion: 'TLSv1',
      },
    },
  },

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
