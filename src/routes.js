import express, { Router } from 'express';
import path from 'path';
import multer from 'multer';
import multerConfig from './config/multer';

import UsuarioController from './app/controllers/UsuarioController';
import ClienteController from './app/controllers/ClienteController';
import MovimentoController from './app/controllers/MovimentoController';
import ItemController from './app/controllers/ItemController';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import authMiddleware from './app/middlewares/auth';
import FinanceiroController from './app/controllers/FinanceiroController';
import ProdutoController from './app/controllers/ProdutoController';
import RomaneioController from './app/controllers/RomaneioController';
import FechamentoNotaController from './app/controllers/FechamentoNotaController';
import RotaController from './app/controllers/RotaController';
import DesossaController from './app/controllers/DesossaController';

const router = new Router();
const upload = multer(multerConfig);

router.post('/usuario', UsuarioController.store);
router.post('/sessions', SessionController.store);

router.put('/coordenadas', RotaController.update);

router.use('/uploads', express.static(path.join(__dirname, '..', 'uploads')));
router.use('/reports', express.static(path.join(__dirname, '..', 'reports')));

router.get(
  '/produtos/composto-desossa/:id',
  DesossaController.getProdutoCompostoPorId
);

router.use(authMiddleware);
router.get('/', (req, res) => res.json({ message: 'Server OK!' }));

router.get('/usuario', UsuarioController.index);

/** Movimentos  */
router.get('/movimentos/', MovimentoController.index);
router.get('/movimento/:id', MovimentoController.show);
router.get('/movimentos/busca/', MovimentoController.show);
router.get('/movimentos/cliente/', MovimentoController.buscaPorCliente);
router.get('/movimentos/novoPedido/', MovimentoController.novoPedido);
router.get(
  '/movimentos/rotas/',
  MovimentoController.buscaMovimentoPorDataRomaneio
);

router.get('/movimentos/pesos', MovimentoController.getPesoAproximadoCaminhao);

router.get('/movimentos/entradas/', MovimentoController.buscaMovimentosEntrada);
router.get(
  '/movimentos/entrada/editaNota/:id/produto/:idPrd',
  MovimentoController.editaMovimentoEntrada
);

router.get(
  '/movimentos/generate-report',
  MovimentoController.gerarRelatorioPDF
);

router.get('/romaneios/', RomaneioController.index);

router.put('/movimentos/romaneios/', MovimentoController.addRomaneioPedido);
router.post('/movimento/entradas/fechamento/', FechamentoNotaController.store);
router.post('/movimentos', MovimentoController.store);
router.post('/movimentos/adicionaItem/', MovimentoController.addItemMovimento);

router.get('/fechamentos', FechamentoNotaController.index);
router.get('/itens/fechamento/', ItemController.findItemsMovDateSlaughter);
router.get('/item/quantidades/', ItemController.findPesos);

router.put('/item/updateProduto', ItemController.updateEstoque);
router.put(
  '/movimento/item/:idMov/sequencial/:numeroSequencialItem',
  ItemController.updateItemMovimento
);

/** Delete */
router.delete('/movimento/:id/delete', MovimentoController.deleteMov);
router.delete(
  '/movimento/item/:idMov/:nseqitmmov/delete',
  MovimentoController.deleteItem
);

router.get('/clientes', ClienteController.index);
router.get('/clientes/praca', ClienteController.buscaClientePorPraca);
router.get('/cliente', ClienteController.buscaPorClientePorCodigo);

router.put('/usuario', UsuarioController.update);
router.post('/files', upload.single('file'), FileController.store);
router.get('/tabela', ProdutoController.index);
router.get('/produto', ProdutoController.show);
router.get('/produto/composto', ProdutoController.findProdutoComposto);

router.get('/financeiro', FinanceiroController.index);
router.get(
  '/financeiro/titulos',
  FinanceiroController.titulosAbertoPorRepresentante
);

export default router;
